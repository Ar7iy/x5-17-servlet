package services;

import dtos.User;
import repos.UserDAO;

import java.util.List;

public class UserService {
    private UserDAO userDAO;

    public UserService(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public boolean userValid(String name, String password) {
        return userDAO.getAllUsers()
                .stream()
                .anyMatch(user -> user.getName().equals(name) && user.getPassword().equals(password));
    }

    public List<User> getUsers() {
        return userDAO.getAllUsers();
    }
}
