package services;

import dtos.Student;
import repos.StudentDAO;

import java.util.List;

public class StudentService {
    private StudentDAO studentDAO;

    public StudentService(StudentDAO studentDAO) {
        this.studentDAO = studentDAO;
    }

    public List<Student> getStudents() {
        return studentDAO.getAllStudents();
    }
}
