import repos.UserDAOSimpleImpl;
import services.UserService;

import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        UserService userService = new UserService(new UserDAOSimpleImpl());
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        String pass = scanner.nextLine();
        System.out.println(userService.userValid(name, pass));
    }
}
