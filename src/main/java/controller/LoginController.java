package controller;

import repos.UserDAOSimpleImpl;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginController extends HttpServlet {

    private UserService userService;

    public LoginController() {
        userService = new UserService(new UserDAOSimpleImpl());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        if (userService.userValid(req.getParameter("name"), req.getParameter("password"))) {
            req.getSession().setAttribute("login", req.getParameter("name"));
            req.getRequestDispatcher("/groups.jsp").forward(req, resp);
        } else {
            req.getRequestDispatcher("/index.jsp").forward(req, resp);
        }
    }
}
