package dtos;

import java.util.Calendar;
import java.util.Date;

public class Group {
    private int id;
    private String name;
    private Calendar graduation;

    @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", graduation=" + graduation +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Calendar getGraduation() {
        return graduation;
    }

    public void setGraduation(Calendar graduation) {
        this.graduation = graduation;
    }

    public Group(int id, String name, Calendar graduation) {
        this.id = id;
        this.name = name;
        this.graduation = graduation;
    }
}
