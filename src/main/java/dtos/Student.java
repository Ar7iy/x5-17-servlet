package dtos;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Student {
    private int id;
    private String name;
    private Calendar birthDate;
    private String city;
    private int groupId;

    public int getGroupId() {
        return groupId;
    }

    public void setGroup(int groupId) {
        this.groupId = groupId;
    }

public Student(int id, String name, Calendar birthDate, String city, int groupId) {
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.city = city;
        this.groupId = groupId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Calendar getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Calendar birthDate) {
        this.birthDate = birthDate;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        Date date = birthDate.getTime();
        SimpleDateFormat birthDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String birth = birthDateFormat.format(date);
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", birthDate=" + birth +
                ", city='" + city + '\'' +
                ", groupId='" + groupId + '\'' +
                '}';
    }
}
