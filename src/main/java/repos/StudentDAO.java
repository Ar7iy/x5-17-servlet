package repos;

import dtos.Student;

import java.util.Calendar;
import java.util.List;

public interface StudentDAO {
    Student createStudent(String name, Calendar birthDate, String city, int groupId);
    void deleteStrudent(int id);
    void updateStudent(int id, String name, Calendar birthDate, String city, int groupId);
    Student getStudent(int id);
    List<Student> getAllStudents();
}
