package repos;

import dtos.Group;
import dtos.Student;

import java.util.*;

public class StudentDAOSimpleImpl implements StudentDAO{

    Map<Integer, Student> students = new HashMap<>();

    public StudentDAOSimpleImpl() {
        students.put(1, new Student(1, "Tom", new GregorianCalendar(2000,0,3), "Moscow", 1));
        students.put(2, new Student(2, "Ben", new GregorianCalendar(2001,1,4), "Saint Petersburg", 2));
        students.put(3, new Student(3, "Gur", new GregorianCalendar(2002,2,5), "Novosibirsk", 1));
    }


    @Override
    public Student createStudent(String name, Calendar birthDate, String city, int groupId) {
        int id = students.size() + 1;
        Student student = new Student(id, name, birthDate, city, groupId);
        students.put(id, student);
        return student;
    }

    @Override
    public void deleteStrudent(int id) {
        students.remove(id);
    }

    @Override
    public void updateStudent(int id, String name, Calendar birthDate, String city, int groupId) {
        students.put(id, new Student(id, name, birthDate, city, groupId));
    }

    @Override
    public Student getStudent(int id) {
        return students.get(id);
    }

    @Override
    public List<Student> getAllStudents() {
        return new ArrayList<Student>(students.values());
    }
}
