package repos;

import dtos.Group;

import java.util.Calendar;

public interface GroupDao {
    Group create(int id, String name, Calendar graduation);
}
