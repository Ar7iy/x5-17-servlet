package repos;

import java.util.List;

public interface SimpleDAO<T> {
    String tableName = "";
    T create(Object... args);
    void delete(int id);
    T update(int id, Object... args);
    T get(int id);
    List<T> getAll();
}
