package repos;

import dtos.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserDAOSimpleImpl implements UserDAO {

    Map<Integer, User> users = new HashMap<>();

    public UserDAOSimpleImpl() {
        users.put(1, new User(1,"1", "1"));
        users.put(2, new User(2,"2", "22"));
        users.put(3, new User(3,"3", "333"));
    }

    @Override
    public User createUser(String name, String password) {
        int id = users.size() + 1;
        User user = new User(id, name, password);
        users.put(id, user);
        return user;
    }

    @Override
    public void deleteUser(int id) {
        users.remove(id);
    }

    @Override
    public void updateUser(int id, String name, String password) {
        User user = new User(id, name, password);
        users.put(id, user);
    }

    @Override
    public User getUser(int id) {
        return users.get(id);
    }

    @Override
    public List<User> getAllUsers() {
        return new ArrayList<User>(users.values());
    }
}
