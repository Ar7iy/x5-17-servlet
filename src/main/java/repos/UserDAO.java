package repos;

import dtos.User;

import java.util.List;

public interface UserDAO {
    User createUser(String name, String password);

    void deleteUser(int id);

    void updateUser(int id, String name, String password);

    User getUser(int id);

    List<User> getAllUsers();
}
